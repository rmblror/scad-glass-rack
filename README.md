# SCAD glass rack

![alt text](img/sideview.png)
![alt text](img/frontview.png)
![alt text](img/backview.png)

This repository contains the design file for creating this rack for wine glasses using a laser cutter.

It is designed parametrically, allowing the user to set the number of glasses, the total size, the various angles, and even fine tune the thicknesses of various aspects of the design. This can be done by changing the variables at the beginning of the file, avoiding the need for any real programming knowledge.

# Making it yourself

To make the rack, first find the laser cutter nearest to you (try fablabs.io). Someone will need to help you operate it, but your part of the task will be to generate the pdf files to "print".
To do this, first open glass_rack.scad in OpenSCAD and adjust the parameters how you like.
Then, comment out line number 380 so that it will not generate the whole model.
One by one, uncomment (remove the "//" at the start of each line) the lines to create the 2D projection of the top plate, the back plate, the side mounts, and the dividers.
For each part, open it in OpenSCAD, press F6 to render, then click "File", "Export", "Export as SVG"
Then open the .svg files in Inkscape or similar in order to arrange them, and export that as the pdf files to be printed.
You can check the cut_designs folder for an example, or to use the pdfs directly if you don't need to change anything.
The model that I made holds 10 glasses and assumes a maximum sheet width of 600mm, which may be too large depending on what laser cutter and materials are available to you.
