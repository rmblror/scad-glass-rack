nglass = 10;
board_thickness_outer = 4.3;
board_thickness_inner = 4.34;
top_angle = 7;
top_width = 585;
top_cut_thickness = 10;
top_cut_hole_diameter = 15;
top_length = 240;
glass_angle = 8;
x_inter_sep = 113;
y_sep = 105;
sidemount_sep = 550;
sidemount_angle = 5;
sidemount_d_over = 15;
sidemount_d_under = 20;
sidemount_end_theta = 45;
sidemount_end_xfrac = 0.5;
sidemount_w = 40;
sidemount_bot_theta = 45;
sidemount_bot_yfrac = 0.67;
sidemount_height = 310;
div_height_back = 310;
div_hb_frac = 0.5;
div_height_mid = 260;
div_hm_frac = 0.4;
div_hf_frac = 0.3;
div_end_width = 20;
div_post_width = 40;
div_mid_theta = 20;
div_cut_offset = 70;
div_nub_factor = 1;
bp_sep = 10;
bp_height = 310;
bp_lower = 50;
mh_rad = 10;
mh_height = 85;
mh_xval = 240;

//Functions to make the glasses
function bowlprofile(t) =
     assert(t >= 0)
     assert(t <= 1)
     t < 0.6 ? //if
	  bezier(t/0.6, [0, 0], [48, 80], f1=[48, 40])
     : //else 
	  bezier((t-0.6)/0.4, [48, 80], [40, 145], f1=[48, 100])
	  ;

module stemprofilemod() {
     polygon( concat([for (t=[0 : 0.1 : 1]) bezier(t, [14, 12], [5, -25], f1=[5, 5]) ],
	  	     [for (t=[0 : 0.1 : 1]) bezier(t, [5, -25], [15, -85], f1=[5, -80]) ],
		     [[0, -85], [0, 10]]));
     }

module baseprofilemod() {
     polygon( concat([for (t=[0 : 0.1 : 1]) bezier(t, [15, -85], [40, -90], f1=[20, -87]) ],
		     [[0, -90], [0, -84]]));
     }

module bowlprofilemod(count=0, resolution=0.001, thickness=1) {
     //Probably not the most elegant way to create a parametric 2D shape with some thickness,
     //but it did the job.
     if (count <= 1/resolution) {
	  union() {
	       translate(bowlprofile(count * resolution)) circle(thickness);
	       bowlprofilemod(count + 1, resolution=resolution, thickness=thickness);
     	  }
	  }
     else {
	  translate(bowlprofile(1)) circle(thickness);
	  }
     }

module hangingglass() {
     //Module for glass hanging upside down. Centered on z axis crossing the xy plane at the hanging point along stem.
     color("whitesmoke") rotate([180, 0 , 0]) translate([0, 0 , 75]) rotate_extrude() union() {
	  intersection() {bowlprofilemod(); translate([300, 0]) square(600, center=true);}
	  stemprofilemod();
	  baseprofilemod();	  
     }
     }

//Functions to set the glasses in place
module putglasses(locs) {
     for (ii = [0 : 1 : len(locs) - 1])
	  translate(locs[ii]) hangingglass();
     }

function getglasslocs(top_angle=0) =
     //Creates list of coordinates where glasses should be. Should be called with the real
     //top angle when placing glasses, and with top_angle=0 when making cuts for top plate
     let(x_inner_sep = tan(glass_angle) * y_sep)
     assert(nglass % 2 == 0)
     [each for (ii = [-1 * (nglass / 2 + 1) / 2 + 1 : 1 : (nglass / 2 + 1) / 2 - 1])
	       [[ii * x_inter_sep + x_inner_sep / 2, -y_sep / 2, -y_sep / 2 * tan(top_angle)],
		[ii * x_inter_sep - x_inner_sep / 2, y_sep / 2, y_sep / 2 * tan(top_angle)]]];

function upnth(locs, n, height=60) =
     //raise the nth glass by height in order to indentify glass indices in the rendering
     [for (ii=[0 : 1 : len(locs) - 1]) ii == n ?
			              [locs[ii][0], locs[ii][1], locs[ii][2] + height]
				      : locs[ii]];

//Functions to make the top plate
module cutpaths(glocs) {
     //paths cut out of top plate
     let(holerad = top_cut_hole_diameter / 2)
     let(cut_radius = top_cut_thickness / 2)
     let(ymaxtop = top_length)
     union() {
     union() {
     for (ii = [0 : 2 : len(glocs) - 2])
	  hull() {
	       translate([glocs[ii + 1][0] - (ymaxtop - glocs[ii + 1][1]) * tan(glass_angle),
			  ymaxtop, 0]) circle(cut_radius);
	       translate(glocs[ii + 1]) circle(cut_radius);
	       }
     for (ii = [0 : 2 : len(glocs) - 2])
	  hull() {
	       translate(glocs[ii]) circle(cut_radius);
	       translate(glocs[ii + 1]) circle(cut_radius);
	       }
     for (ii = [0 : 1 : len(glocs) - 1])
	  translate(glocs[ii]) circle(holerad);
     
     rotate([0, 0, 90])
     union() {
	  translate([0, -sidemount_sep / 2, 0])
	  rotate([0, 0, sidemount_angle])
	  translate([-top_length/2, 0, 0])
	  square([top_length, board_thickness_inner], center=true);
	  translate([0, sidemount_sep / 2, 0])
	  rotate([0, 0, -sidemount_angle])
	  translate([-top_length/2, 0, 0])
	  square([top_length, board_thickness_inner], center=true);
	  }
     }
     for (xloc = [-x_inter_sep * (len(glocs) / 4 - 1) : x_inter_sep :
		  x_inter_sep * (len(glocs) / 4 - 1)])
     translate([xloc, 0, 0])
     rotate([0, 0, glass_angle])
     translate([0, top_length / 2 - (div_post_width / 2 + div_end_width) * cos(glass_angle), 0]) 
     square([board_thickness_inner, div_post_width], center=true);
     }
     }

module top_plate() {
     color("saddlebrown") rotate([top_angle,0,0])
     translate([0, 0, -board_thickness_outer / 2])
     linear_extrude(board_thickness_outer)
     difference() {square([top_width, top_length], center=true);
     cutpaths(getglasslocs());}
}

//Functions to make the dividers between glass rows
function divider_nub_points() =
    //points for the nub that fits in the holes in the top plate,
    //added to the profile inside of polygon() 
    let (pt_end = [top_length / 2 / cos(glass_angle) * cos(top_angle), top_length / 2 *
		   sin(top_angle)])
    let (pt_top_far = [pt_end[0] - div_end_width * cos(top_angle),
		       pt_end[1] - div_end_width * sin(top_angle)])
    let (pt_top_close = [pt_end[0] - (div_end_width + div_post_width) * cos(top_angle),
		       pt_end[1] - (div_end_width + div_post_width) * sin(top_angle)])
    [pt_top_close,
     [pt_top_close[0] - div_nub_factor*board_thickness_outer * sin(top_angle),
      pt_top_close[1] + div_nub_factor*board_thickness_outer * cos(top_angle)],
     [pt_top_far[0] - div_nub_factor*board_thickness_outer * sin(top_angle),
      pt_top_far[1] + div_nub_factor*board_thickness_outer * cos(top_angle)],
     pt_top_far];

function divider_profile(t) =
    //Define all the relevant points in terms of the sidemount_ variables, then
    //parametrically trace out all of the bezier curves
    let (pt_end = [top_length / 2 / cos(glass_angle) * cos(top_angle), top_length / 2 *
		   sin(top_angle)])
    let (pt_top_far = [pt_end[0] - div_end_width * cos(top_angle),
		       pt_end[1] - div_end_width * sin(top_angle)])
    let (pt_top_close = [pt_end[0] - (div_end_width + div_post_width) * cos(top_angle),
		       pt_end[1] - (div_end_width + div_post_width) * sin(top_angle)])
    let (pt_end_close = [pt_end[0] - (2*div_end_width + div_post_width) * cos(top_angle),
		       pt_end[1] - (2*div_end_width + div_post_width) * sin(top_angle)])
    let (pt_under_far = [pt_top_far[0], pt_top_far[1] - div_height_mid * div_hf_frac])
    let (pt_over_far = [pt_end_close[0], pt_end_close[1] - div_height_mid * div_hf_frac])
    let (theta_far = atan((pt_top_close[1] - pt_over_far[1]) / (pt_top_close[0] - pt_over_far[0])))
    let (pt_over_mid = [0, -1 * div_height_mid * div_hm_frac])
    let (pt_under_mid = [0, -1 * div_height_mid])
    let (pt_over_back = [-top_length / 2 / cos(glass_angle) * cos(top_angle),
			 -top_length / 2 * sin(top_angle) - div_height_back * div_hb_frac])
    let (pt_under_back = [-top_length / 2 / cos(glass_angle) * cos(top_angle),
			 -top_length / 2 * sin(top_angle) - div_height_back])
    let (focus_over_back = [pt_over_back[0],
			    pt_over_back[0] * tan(div_mid_theta) + pt_over_mid[1]])
    let (focus_under_back = [pt_under_back[0],
			     pt_under_back[0] * tan(div_mid_theta) + pt_under_mid[1]])
    let (focus_over_far = intersect_2d(pt_over_mid, div_mid_theta, pt_over_far, theta_far))
    let (focus_under_far = intersect_2d(pt_under_far, 90, pt_under_mid, div_mid_theta))
    t < 1 ?
    bezier(t, pt_end, pt_under_far, f1=pt_top_far)
    :
    t < 2 ?
     bezier(t-1, pt_under_far, pt_under_mid, f1=focus_under_far)
    :
    t < 3 ?
     bezier(t-2, pt_under_mid, pt_under_back, f1=focus_under_back)
    :
    t < 4 ?
     bezier(t-3, pt_over_back, pt_over_mid, f1=focus_over_back)
    :
    t < 5 ?
     bezier(t-4, pt_over_mid, pt_over_far, f1=focus_over_far)
    :
     bezier(t-5, pt_over_far, pt_end_close, f1=pt_top_close)
    ;
			    
module divider() {
     color("saddlebrown")
     rotate([0, 0, 90])
     rotate([90, 0, 0])
     translate([0, 0, -board_thickness_outer / 2])
     linear_extrude(board_thickness_outer)
     difference() {
     polygon(concat([for (t=[0 : 0.05 : 6]) divider_profile(t)], divider_nub_points()));
     translate([(-top_length / 2 * cos(top_angle) + bp_sep)  / cos(glass_angle),
		-500 - (div_height_back * (1 - div_hb_frac) +
		 top_length / 2 * sin(top_angle) + div_cut_offset), 0])
     square([board_thickness_inner, 1000], center=true);
     }
     }

module all_dividers(glocs=getglasslocs(), x_inter_sep=x_inter_sep) {
     for (xloc = [-x_inter_sep * (len(glocs) / 4 - 1) : x_inter_sep :
		  x_inter_sep * (len(glocs) / 4 - 1)])
	  translate([xloc, 0, 0])
	  rotate([0, 0, atan(tan(glass_angle)/cos(top_angle))])
	  divider();
     }

//Back plate
module divider_cuts(glocs=getglasslocs()) {
     for (xloc = [-x_inter_sep * (len(glocs) / 4 - 1) : x_inter_sep : x_inter_sep *
		  (len(glocs) / 4 - 1)])
     translate([xloc + (top_length / 2 * cos(top_angle) - bp_sep) * tan(glass_angle),
		-500 + div_height_back * (1 - div_hb_frac) +
		top_length / 2 * sin(top_angle) + div_cut_offset, 0])
     square([board_thickness_inner / cos(glass_angle), 1000], center=true);
     }

module mount_holes() {
       translate([0, mh_height, 0]) circle(mh_rad);
       translate([mh_xval, mh_height, 0]) circle(mh_rad);
       translate([-mh_xval, mh_height, 0]) circle(mh_rad);
       }

module back_plate() {
     color("saddlebrown")
     translate([0, -top_length / 2 + bp_sep, 0])
     rotate([-90, 0, 0])
     linear_extrude(board_thickness_outer)
     difference() {
     difference() {
     translate([0, (bp_height + bp_lower) / 2, 0])
     square([top_width, bp_height - bp_lower], center=true);
     union() {
     union() {
	  translate([-(sidemount_sep + (top_length - bp_sep)  *tan(sidemount_angle)) / 2, 0, 0])
	  square([board_thickness_inner / cos(sidemount_angle), sidemount_height], center=true);
	  translate([(sidemount_sep + (top_length - bp_sep) * tan(sidemount_angle)) / 2, 0, 0])
	  square([board_thickness_inner / cos(sidemount_angle), sidemount_height], center=true);
     }
     divider_cuts();
     }
     }
     mount_holes() ;
     }
     }

//Geometry
function bezier(t, start, end, order=2, f1=[0,0,0], f2=[0,0,0], f3=[0,0,0]) =
     assert(order > 1, "bezier curve must be 2nd order or higher")
     assert(order < 3, "higher order than 2 not implemented, 
                        but could be someday by some brave soul")
     assert(t >= 0)
     assert(t <= 1)
     order == 2 ?
         (1 - t) * ((1 - t) * start + t * f1) + t*((1 - t) * f1 + t * end)
     :
         pow(1 - t, 3)*start + 3*pow(1 - t, 2) * t * f1 +
         3 * pow(1 - t, 2) * t * f2 + pow(t, 3) * end
     ;

function para_line(t, start, end) = 
     start + (1 - t) * (end - start);

function intersect_2d(pt0, th0, pt1, th1) =
    //Get the intersection point between two lines (2D) given by point and angle
    let (A = [[cos(th0), -cos(th1)], [sin(th0), -sin(th1)]])
    let (detA = A[0][0] * A[1][1] - A[1][0] * A[0][1])
    let (invA = [[A[1][1], -1 * A[0][1]], [-1 * A[1][0], A[0][0]]] / detA)
    let (distances = invA * [pt1[0] - pt0[0], pt1[1] - pt0[1]])
    let (from1 = pt0 + [cos(th0), sin(th0)] * distances[0])
    from1;

//Functions to make the side supports connecting the top and the back plate
function side_mount_profile(t) =
    //Same idea as divider_profile
    let (pt_end = [top_length / 2 / cos(sidemount_angle) * cos(top_angle), top_length / 2 *
		   sin(top_angle)])
    let (pt_x_mid_under = [(sidemount_end_xfrac - 0.5) * top_length / 2 / cos(sidemount_angle) *
			    cos(top_angle),
			    (sidemount_end_xfrac - 0.5) * top_length / 2 * sin(top_angle) -
			    sidemount_d_under * cos(top_angle)])
    let (pt_x_mid_over = [(sidemount_end_xfrac - 0.5) * top_length / 2 / cos(sidemount_angle) *
			    cos(top_angle),
			    (sidemount_end_xfrac - 0.5) * top_length / 2 * sin(top_angle) +
			    sidemount_d_over * cos(top_angle)])
    let (pt_back_top = [-top_length / 2 / cos(sidemount_angle) * cos(top_angle),
			(-top_length / 2 / cos(sidemount_angle) * sin(top_angle) + 
			 sidemount_d_over * cos(top_angle) + pt_x_mid_over[1]) / 2])
    let (pt_bot = [-1 * top_length / 2 / cos(sidemount_angle) * cos(top_angle),
	            -1 * top_length / 2 * sin(top_angle) - sidemount_height])
    let (pt_y_mid = [(-1 * top_length / 2 * cos(top_angle) + sidemount_w) / cos(sidemount_angle),
		     -1 * top_length / 2 * sin(top_angle) - sidemount_bot_yfrac *
		     sidemount_height])
    let (focus_end_under = intersect_2d(pt_end, (sidemount_end_theta + top_angle),
					pt_x_mid_under, (top_angle)))
    let (focus_end_over = intersect_2d(pt_end, (-1 * sidemount_end_theta + top_angle),
				       pt_x_mid_over, (top_angle)))
    let (focus_back_over = intersect_2d(pt_x_mid_over, (top_angle), pt_back_top, 0))
    let (focus_mid = intersect_2d(pt_x_mid_under, top_angle, pt_y_mid, 90))
    let (focus_bot = intersect_2d(pt_y_mid, 90, pt_bot, sidemount_bot_theta))
    t < 1 ?
     bezier(t, pt_back_top, pt_x_mid_over, f1=focus_back_over)
    :
    t < 2 ?
     bezier(t-1, pt_x_mid_over, pt_end, f1=focus_end_over)
    :
    t < 3 ?
     bezier(t-2, pt_end, pt_x_mid_under, f1=focus_end_under)
    :
    t < 4 ?
     bezier(t-3, pt_x_mid_under, pt_y_mid, f1=focus_mid)
    :
     bezier(t-4, pt_y_mid, pt_bot, f1=focus_bot);

module side_mount() {
     color("saddlebrown")
     rotate([0, 0, 90])
     rotate([90, 0, 0])
     translate([0, 0, -board_thickness_inner / 2])
     linear_extrude(board_thickness_inner)
     difference(){
     difference(){
     polygon([for (t=[0 : 0.05 : 5]) side_mount_profile(t)]);
     rotate([0, 0, top_angle])
     translate([250, 0, 0])
     square([500, board_thickness_inner], center=true);}
     translate([-top_length / 2 / cos(sidemount_angle) * cos(top_angle) + bp_sep,
		-sidemount_height, 0])
     square([board_thickness_inner, sidemount_height], center=true);}
     }

module all_side_mounts(sidemount_sep=sidemount_sep) {
     for (ii = [-1, 1])
	  translate([sidemount_sep / 2 * ii, 0, 0])
	  rotate([0, 0, ii * atan(tan(sidemount_angle)/cos(top_angle))])
	  side_mount();
     }

module model() {
     //The whole thing
       putglasses(getglasslocs(top_angle=top_angle));
       all_side_mounts();
       top_plate();
       all_dividers();
       back_plate();
       }
       
//Put "//" in front of the following line in order to prevent OpenSCAD from rendering the model.
model();

//To make the individual pieces, uncomment one of these lines to make the 2D projection.
//In OpenSCAD, render, export as svg, then open in inkscape to arrange and save as pdf.

//projection() rotate([90, 0, 0]) back_plate();
//projection() rotate([-top_angle, 0, 0]) top_plate();
//projection() rotate([0, 90, 0]) divider();
//projection() rotate([0, 90, 0]) side_mount();
